module.exports = {
    bracketSpacing: true,
    jsxBracketSameLine: false,
    singleQuote: true,
    trailingComma: 'es5',
    printWidth: 110,
    tabWidth: 4,
    useTabs: false,
};
