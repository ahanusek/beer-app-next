import React from 'react';
import { Provider } from 'react-redux';
import { StatusBar, StyleSheet, SafeAreaView } from 'react-native';
import { DefaultTheme, Provider as PaperProvider, ThemeShape } from 'react-native-paper';
import store from './src/store/store';
import MainNavigator from './src/navigation';
import { Colors, Sizes } from './src/theme';

const theme: ThemeShape = {
    ...DefaultTheme,
    roundness: Sizes.borderRadius,
    colors: {
        ...DefaultTheme.colors,
        primary: Colors.primary1,
        accent: Colors.secondary1,
        disabled: Colors.disabled,
    },
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

const App = () => {
    return (
        <SafeAreaView style={styles.container}>
            <Provider store={store}>
                <PaperProvider theme={theme}>
                    <StatusBar backgroundColor="transparent" translucent />
                    <MainNavigator />
                </PaperProvider>
            </Provider>
        </SafeAreaView>
    );
};

export default App;
