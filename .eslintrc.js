module.exports = {
    root: true,
    extends: ['react-native-wcandillon', 'plugin:import/errors', 'plugin:import/typescript'],
    env: { jest: true },
    rules: {
        quotes: ['error', 'single'],
        'no-console': 2,
        'sort-imports': 0,
        'no-bitwise': 0,
        'react/jsx-indent': [2, 4],
        'react/jsx-indent-props': 0,
        'react/prop-types': 0,
        'import/prefer-default-export': 0,
        'jsx-a11y/anchor-is-valid': 0,
        '@typescript-eslint/no-object-literal-type-assertion': 0,
        '@typescript-eslint/camelcase': 0,
        'max-len': ['error', { code: 110, ignoreTemplateLiterals: true, ignoreStrings: true }],
        'react/jsx-wrap-multilines': 0,
        'no-underscore-dangle': 0,
        'react/destructuring-assignment': ['error', 'always', { ignoreClassFields: true }],
        'react/jsx-one-expression-per-line': 0,
        'import/no-extraneous-dependencies': 0,
    },
    settings: {
        'import/resolver': {
            node: {
                extensions: ['.js', '.jsx', '.ts', '.tsx', '.d.ts'],
            },
        },
    },
};
