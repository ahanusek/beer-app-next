module.exports = {
    preset: 'react-native',
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
    setupFiles: ['./jestSetupFile.js'],
    testPathIgnorePatterns: ['/node_modules/', '/e2e/'],
};
