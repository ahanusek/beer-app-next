interface Ingredient {
    amount: Amount;
    name: string;
}

type Malt = Ingredient;

interface Hop extends Ingredient {
    add: string;
    attribute?: string;
}

interface Ingredients {
    hops: Hop[];
    malt: Malt[];
    yeast: string;
}

interface Amount {
    value: number;
    unit: string;
}

export interface Beer {
    abv: number;
    attenuation_level: number;
    boil_volume: Amount;
    brewers_tips: string;
    contributed_by: string;
    description: string;
    ebc: number;
    first_brewed: string;
    food_pairing: string[];
    ibu: number;
    id: number;
    image_url: string | null;
    ingredients: Ingredients;
    method: {
        [key: string]: {
            duration?: number;
            twist?: null | string;
            temp: Amount;
        };
    };
    name: string;
    ph: number;
    srm: number;
    tagline: string;
    target_fg: number;
    target_og: number;
    volume: Amount;
}
