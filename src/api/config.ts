import axios from 'axios';

const API_URL = 'https://api.punkapi.com/v2/';
export const DEFAULT_PER_PAGE = 10;

const axiosInstance = axios.create({
    baseURL: API_URL,
});

export default axiosInstance;
