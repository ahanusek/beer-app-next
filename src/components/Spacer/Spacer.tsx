import React, { FunctionComponent } from 'react';
import { View } from 'react-native';

interface Props {
    size: number;
}

const Spacer: FunctionComponent<Props> = ({ size = 10 }) => (
    <View
        style={{
            left: 0,
            right: 0,
            height: 1,
            marginTop: size - 1,
        }}
    />
);

export default Spacer;
