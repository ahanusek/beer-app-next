import React from 'react';
import { render } from 'react-native-testing-library';
import Alert from './Alert';

describe('Alert component', () => {
    const props = {
        message: 'Dummy message',
    };
    it('should render alert with message', () => {
        const { getByText } = render(<Alert {...props} />);
        expect(getByText(props.message)).toBeTruthy();
    });
    it('should render loader when alert type is loading', () => {
        const { update, queryByName } = render(<Alert {...props} />);
        expect(queryByName('ActivityIndicator')).toBeNull();
        update(<Alert {...props} type="loading" />);
        expect(queryByName('ActivityIndicator')).toBeTruthy();
    });
});
