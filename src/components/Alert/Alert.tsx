import React, { FunctionComponent } from 'react';
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Sizes, Colors } from '../../theme';

interface OwnProps {
    message: string;
    type?: 'success' | 'error' | 'warning' | 'loading';
    testID?: string;
}

type Props = OwnProps;

const styles = StyleSheet.create({
    container: {
        marginHorizontal: Sizes.margin,
        borderRadius: Sizes.borderRadius,
        marginVertical: 10,
    },
    messageContainer: {
        paddingVertical: 10,
        paddingHorizontal: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    message: {
        textAlign: 'center',
        color: Colors.light,
    },
    loader: {
        marginHorizontal: 10,
    },
});

const gradients = {
    error: Colors.gradients.third,
    warning: Colors.gradients.fourth,
    success: Colors.gradients.primary,
    loading: Colors.gradients.fourth,
};

const Alert: FunctionComponent<Props> = ({ message, type = 'error', testID }) => {
    return (
        <LinearGradient
            colors={gradients[type]}
            start={{ x: 0, y: 0.5 }}
            end={{ x: 1, y: 0.5 }}
            style={styles.container}
        >
            <View style={styles.messageContainer} testID={testID}>
                {type === 'loading' ? (
                    <ActivityIndicator size="small" color={Colors.light} style={styles.loader} />
                ) : null}
                <Text style={styles.message} testID="alert-message">
                    {message}
                </Text>
            </View>
        </LinearGradient>
    );
};

export default Alert;
