import { createAppContainer } from 'react-navigation';
import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { BeersList, FavouriteBeers, BeerDetails } from '../screens';
import { Colors } from '../theme';

const BeerListStack = createStackNavigator({
    BeersList: {
        screen: BeersList,
        navigationOptions: {
            header: null,
        },
    },
    BeerDetails: {
        screen: BeerDetails,
        navigationOptions: {
            title: 'Beer details',
        },
    },
});

const FavouriteBeersStack = createStackNavigator({
    FavouriteBeers: {
        screen: FavouriteBeers,
        navigationOptions: {
            header: null,
        },
    },
    BeerDetails: {
        screen: BeerDetails,
        navigationOptions: {
            title: 'Beer details',
        },
    },
});

const MainNavigator = createBottomTabNavigator(
    {
        BeerListStack: {
            screen: BeerListStack,
            navigationOptions: {
                tabBarTestID: 'beers-list-tab',
                tabBarIcon: ({ focused }) => (
                    <Icon name="beer" size={28} color={focused ? Colors.primary1 : Colors.primary3} />
                ),
            },
        },
        FavouriteBeersStack: {
            screen: FavouriteBeersStack,
            navigationOptions: {
                tabBarTestID: 'favs-list-tab',
                tabBarIcon: ({ focused }) => (
                    <Icon name="cards-heart" size={28} color={focused ? Colors.primary1 : Colors.primary3} />
                ),
            },
        },
    },
    {
        navigationOptions: {},
        tabBarOptions: {
            showLabel: false,
        },
    }
);

export default createAppContainer(MainNavigator);
