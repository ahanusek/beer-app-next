import React, { FunctionComponent, useEffect } from 'react';
import { View, StyleSheet, FlatList, ActivityIndicator } from 'react-native';
import { isEmpty } from 'lodash';
import { connect } from 'react-redux';
import { Button } from 'react-native-paper';
import { NavigationScreenProp } from 'react-navigation';
import { AppStore } from '../../store/types';
import { fetchBeers } from '../../store/beers/actions/fetchBeers/fetchBeers';
import { RequestStatus } from '../../enums/requestStatus';
import { BeerCard } from './components';
import { BeersReducer } from '../../store/beers/types';
import { Alert } from '../../components';
import { fetchMoreBeers } from '../../store/beers/actions/fetchMoreBeers/fetchMoreBeers';
import { addBeerToFavourite } from '../../store/favouriteBeers/actions/addBeerToFavourite/addBeerToFavourite';
import { removeBeerFromFavourite } from '../../store/favouriteBeers/actions/removeBeerFromFavourite/removeBeerFromFavourite';
import { AppStyles } from '../../theme';
import { Beer } from '../../api/types';
import { FavouriteBeersReducer } from '../../store/favouriteBeers/types';

export interface BeersListProps {
    beers: BeersReducer;
    fetchBeersAction: (page?: number) => void;
    fetchMoreBeersAction: () => void;
    favouriteBeers: FavouriteBeersReducer;
    navigation: NavigationScreenProp<{}>;
    addBeerToFavouriteAction: (data: Beer) => void;
    removeBeerFromFavouriteAction: (id: number) => void;
}

type Props = BeersListProps;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20,
    },
    list: {
        justifyContent: 'center',
    },
    loaderContainer: {
        flex: 1,
        justifyContent: 'center',
        marginTop: '30%',
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10,
    },
});

export const BeersList: FunctionComponent<Props> = ({
    fetchBeersAction,
    beers,
    fetchMoreBeersAction,
    navigation,
    favouriteBeers,
    addBeerToFavouriteAction,
    removeBeerFromFavouriteAction,
}) => {
    useEffect(() => {
        fetchBeersAction();
    }, [fetchBeersAction]);
    const getCardProps = (selectedBeer: Beer) => {
        if (favouriteBeers.data.find(beer => beer.id === selectedBeer.id)) {
            return {
                isFavourite: true,
                toggleFavourite: () => removeBeerFromFavouriteAction(selectedBeer.id),
            };
        }
        return {
            isFavourite: false,
            toggleFavourite: () => addBeerToFavouriteAction(selectedBeer),
        };
    };
    const renderContent = () => {
        const shouldFetchNextPage = !isEmpty(beers.data) && !beers.fetchingMore && !beers.fetchedAll;
        if (beers.status === RequestStatus.PENDING && isEmpty(beers.data)) {
            return (
                <View style={[styles.loaderContainer, styles.horizontal]}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            );
        }
        if (beers.status === RequestStatus.ERROR) {
            return (
                <View>
                    <Alert message="Error occurred when fetching beers list. You should refresh list and try again fetch data." />
                    <Button onPress={() => fetchBeersAction(1)} style={AppStyles.paddingHorizontal}>
                        Refresh
                    </Button>
                </View>
            );
        }
        return (
            <FlatList
                keyExtractor={item => item.id.toString()}
                data={beers.data}
                refreshing={beers.status === RequestStatus.PENDING}
                renderItem={({ item }: { item: Beer }) => (
                    <BeerCard
                        beer={item}
                        seeDetails={() => navigation.navigate('BeerDetails', { beer: item })}
                        {...getCardProps(item)}
                    />
                )}
                ListEmptyComponent={
                    <Alert type="warning" message="List of beers is empty. Please contact with support." />
                }
                onEndReached={shouldFetchNextPage ? fetchMoreBeersAction : undefined}
                onRefresh={fetchBeersAction}
            />
        );
    };
    return (
        <View style={styles.container} testID="beers-list">
            <View style={styles.list}>{renderContent()}</View>
        </View>
    );
};

const mapStateToProps = (store: AppStore) => ({
    beers: store.beers,
    favouriteBeers: store.favouriteBeers,
});

export default connect(
    mapStateToProps,
    {
        fetchBeersAction: fetchBeers,
        fetchMoreBeersAction: fetchMoreBeers,
        addBeerToFavouriteAction: addBeerToFavourite,
        removeBeerFromFavouriteAction: removeBeerFromFavourite,
    }
)(BeersList);
