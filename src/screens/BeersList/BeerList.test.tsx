import React from 'react';
import { render, fireEvent } from 'react-native-testing-library';
import { BeersListProps, BeersList } from './BeersList';
import { RequestStatus } from '../../enums/requestStatus';

describe('BeersList screen', () => {
    let props: BeersListProps;
    beforeEach(() => {
        props = {
            navigation: {
                navigate: jest.fn(),
                //  mocking whole beer object would be painful :P
                // eslint-disable-next-line
            } as any,
            fetchBeersAction: jest.fn(),
            addBeerToFavouriteAction: jest.fn(),
            removeBeerFromFavouriteAction: jest.fn(),
            fetchMoreBeersAction: jest.fn(),
            beers: {
                data: [],
                status: RequestStatus.PENDING,
                fetchedAll: false,
                fetchingMore: false,
                page: null,
                error: null,
            },
            favouriteBeers: {
                data: [],
                error: null,
                processing: false,
                status: null,
            },
        };
    });
    it('should invoke fetchBeer on mount and show loader when fetch initial data', () => {
        const { queryByName } = render(<BeersList {...props} />);
        expect(props.fetchBeersAction).toHaveBeenCalledTimes(1);
        expect(queryByName('ActivityIndicator')).toBeTruthy();
    });
    it('should show info about empty list when fetched data is empty array', () => {
        const { queryByName, update, queryByText } = render(<BeersList {...props} />);
        const beers = {
            ...props.beers,
            status: RequestStatus.SUCCESS,
        };
        expect(props.fetchBeersAction).toHaveBeenCalledTimes(1);
        expect(queryByName('ActivityIndicator')).toBeTruthy();
        expect(queryByName('Alert')).toBeFalsy();
        update(<BeersList {...props} beers={beers} />);
        expect(queryByName('ActivityIndicator')).toBeFalsy();
        expect(queryByText('List of beers is empty. Please contact with support.')).toBeTruthy();
        expect(queryByName('Alert')).toBeTruthy();
    });
    it('should show error message when fetching list have failed', () => {
        const beers = {
            ...props.beers,
            status: RequestStatus.ERROR,
        };
        const { queryByName, queryByText } = render(<BeersList {...props} beers={beers} />);
        expect(queryByName('Alert')).toBeTruthy();
        expect(
            queryByText(
                'Error occurred when fetching beers list. You should refresh list and try again fetch data.'
            )
        ).toBeTruthy();
    });
    it('should show two BeerCard when have fetched list with two items', () => {
        const beers = {
            ...props.beers,
            status: RequestStatus.SUCCESS,
            data: [
                {
                    id: 1,
                    name: 'Test',
                    tagline: 'Test2',
                },
                {
                    id: 2,
                    name: 'Test2',
                    tagline: 'Test2',
                },
                // eslint-disable-next-line
            ] as any,
        };
        const { queryAllByName } = render(<BeersList {...props} beers={beers} />);
        expect(queryAllByName('BeerCard')).toHaveLength(2);
    });
    it('should invoke navigate func after clicking on see details button in BeerCard', () => {
        const beers = {
            ...props.beers,
            status: RequestStatus.SUCCESS,
            data: [
                {
                    id: 1,
                    name: 'Test',
                    tagline: 'Test2',
                },
                {
                    id: 2,
                    name: 'Test2',
                    tagline: 'Test2',
                },
                // eslint-disable-next-line
            ] as any,
        };
        const { getAllByTestId } = render(<BeersList {...props} beers={beers} />);
        fireEvent(getAllByTestId('see-details')[0], 'onPress');
        expect(props.navigation.navigate).toHaveBeenCalledTimes(1);
        expect(props.navigation.navigate).toHaveBeenCalledWith('BeerDetails', { beer: beers.data[0] });
    });
});
