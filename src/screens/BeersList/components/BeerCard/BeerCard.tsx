import React, { FunctionComponent, memo } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { Button, IconButton, Colors as PaperColors } from 'react-native-paper';
import { AppStyles, Colors, Sizes } from '../../../../theme';
import { Spacer } from '../../../../components';
import { Beer } from '../../../../api/types';

interface OwnProps {
    beer: Beer;
    isFavourite: boolean;
    seeDetails: (beer: Beer) => void;
    toggleFavourite: () => void;
}

type Props = OwnProps;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        margin: 20,
        backgroundColor: Colors.primary5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: Sizes.borderRadius,
        ...AppStyles.shadow,
        position: 'relative',
    },
    title: {
        color: Colors.primary1,
    },
    image: {
        height: 150,
        width: 150,
        marginTop: 10,
        marginBottom: 10,
    },
    favContainer: {
        position: 'absolute',
        right: 20,
        top: 10,
    },
});

const BeerCard: FunctionComponent<Props> = ({ beer, isFavourite, seeDetails, toggleFavourite }) => (
    <View style={styles.container} testID={`beer-card-${beer.id}`}>
        <IconButton
            icon={isFavourite ? 'favorite' : 'favorite-border'}
            color={PaperColors.red500}
            size={25}
            style={styles.favContainer}
            onPress={toggleFavourite}
            testID="icon-button"
        />
        <Image
            resizeMode="contain"
            style={styles.image}
            source={
                beer.image_url
                    ? { uri: beer.image_url }
                    : require('../../../../assets/images/image-placeholder.jpg')
            }
            loadingIndicatorSource={require('../../../../assets/images/image-placeholder.jpg')}
        />
        <Text style={[AppStyles.h3, styles.title]}>{beer.name}</Text>
        <Text>{beer.tagline}</Text>
        <Spacer size={20} />
        <Button mode="contained" onPress={() => seeDetails(beer)} testID="see-details">
            See details
        </Button>
        <Spacer size={10} />
    </View>
);

export default memo(BeerCard);
