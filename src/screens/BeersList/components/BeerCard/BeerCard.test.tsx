import React from 'react';
import { render, fireEvent } from 'react-native-testing-library';
import BeerCard from './BeerCard';

describe('BeerCard component', () => {
    const props = {
        // eslint-disable-next-line
        beer: { name: 'test', tagline: 'test1' } as any,
        isFavourite: false,
        seeDetails: jest.fn(),
        toggleFavourite: jest.fn(),
    };
    it('should render proper icon when beer doesnt belong to favs', () => {
        const { getByTestId } = render(<BeerCard {...props} />);
        const IconButton = getByTestId('icon-button');
        expect(IconButton.props.icon).toBe('favorite-border');
    });
    it('should render proper icon when beer belongs to favs', () => {
        const { getByTestId } = render(<BeerCard {...props} isFavourite />);
        const IconButton = getByTestId('icon-button');
        expect(IconButton.props.icon).toBe('favorite');
    });
    it('should invoke toggleFavourite func after clicking on IconButton', () => {
        const { getByTestId } = render(<BeerCard {...props} isFavourite />);
        expect(props.toggleFavourite).toHaveBeenCalledTimes(0);
        fireEvent(getByTestId('icon-button'), 'onPress');
        expect(props.toggleFavourite).toHaveBeenCalledTimes(1);
    });
    it('should invoke seeDetails func after clicking on see details button', () => {
        const { getByTestId } = render(<BeerCard {...props} isFavourite />);
        expect(props.seeDetails).toHaveBeenCalledTimes(0);
        fireEvent(getByTestId('see-details'), 'onPress');
        expect(props.seeDetails).toHaveBeenCalledTimes(1);
    });
});
