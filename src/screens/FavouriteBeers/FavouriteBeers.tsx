import React, { FunctionComponent, useEffect } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { NavigationScreenProp } from 'react-navigation';
import { fetchFavouriteBeers } from '../../store/favouriteBeers/actions/fetchFavouriteBeers/fetchFavouriteBeers';
import { AppStore } from '../../store/types';
import { FavouriteBeersReducer } from '../../store/favouriteBeers/types';
import { Beer } from '../../api/types';
import { BeerCard } from '../BeersList/components';
import { Alert } from '../../components';
import { removeBeerFromFavourite } from '../../store/favouriteBeers/actions/removeBeerFromFavourite/removeBeerFromFavourite';

interface OwnProps {
    favouriteBeers: FavouriteBeersReducer;
    fetchFavouriteBeersAction: () => void;
    removeBeerFromFavouriteAction: (id: number) => void;
    navigation: NavigationScreenProp<{}>;
}

type Props = OwnProps;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20,
    },
});

const FavouriteBeers: FunctionComponent<Props> = ({
    fetchFavouriteBeersAction,
    favouriteBeers,
    navigation,
    removeBeerFromFavouriteAction,
}) => {
    useEffect(() => {
        if (!favouriteBeers.status) {
            fetchFavouriteBeersAction();
        }
    }, [fetchFavouriteBeersAction, favouriteBeers.status]);
    return (
        <View style={styles.container} testID="favourite-beers">
            <FlatList
                keyExtractor={item => item.id.toString()}
                data={favouriteBeers.data}
                renderItem={({ item }: { item: Beer }) => (
                    <BeerCard
                        beer={item}
                        seeDetails={() => navigation.navigate('BeerDetails', { beer: item })}
                        isFavourite
                        toggleFavourite={() => removeBeerFromFavouriteAction(item.id)}
                    />
                )}
                ListEmptyComponent={
                    <Alert
                        type="warning"
                        message="List of favourite beers is empty. Please add new beer to this list."
                        testID="empty-favs-list"
                    />
                }
            />
        </View>
    );
};

const mapStateToProps = (store: AppStore) => ({
    favouriteBeers: store.favouriteBeers,
});

export default connect(
    mapStateToProps,
    { fetchFavouriteBeersAction: fetchFavouriteBeers, removeBeerFromFavouriteAction: removeBeerFromFavourite }
)(FavouriteBeers);
