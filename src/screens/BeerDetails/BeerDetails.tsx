import React, { FunctionComponent, useEffect } from 'react';
import { View, Text, StyleSheet, ScrollView, Image } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { connect } from 'react-redux';
import { ActivityIndicator, Button } from 'react-native-paper';
import { Spacer } from '../../components';
import { Beer } from '../../api/types';
import { Colors, Sizes, AppStyles } from '../../theme';
import { addBeerToFavourite } from '../../store/favouriteBeers/actions/addBeerToFavourite/addBeerToFavourite';
import { removeBeerFromFavourite } from '../../store/favouriteBeers/actions/removeBeerFromFavourite/removeBeerFromFavourite';
import { AppStore } from '../../store/types';
import { SimilarBeersReducer } from '../../store/similarBeers/types';
import { RequestStatus } from '../../enums/requestStatus';
import { fetchSimilarBeers } from '../../store/similarBeers/actions/fetchSimilarBeers/fetchSimilarBeers';

interface OwnProps {
    navigation: NavigationScreenProp<{ beer: Beer }>;
    addBeerToFavouriteAction: (data: Beer) => void;
    removeBeerFromFavouriteAction: (id: number) => void;
    favouriteBeers: Beer[];
    similarBeers: SimilarBeersReducer;
    fetchSimilarBeersAction: (ibu: number, id: number) => void;
}

type Props = OwnProps;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: Sizes.padding,
        backgroundColor: Colors.background,
    },
    headerContainer: {
        flexDirection: 'row',
    },
    header: {
        width: 0,
        flexGrow: 1,
    },
    title: {
        color: Colors.primary1,
        marginTop: 10,
    },
    image: {
        height: 150,
        width: 100,
        marginTop: 10,
        marginBottom: 10,
    },
    tagline: {
        paddingRight: 5,
    },
    rate: {
        marginBottom: 5,
    },
    greyText: {
        color: Colors.textSecondary,
        fontWeight: '700',
    },
    textContainer: {
        padding: Sizes.padding,
    },
    bestOptions: {
        marginTop: 5,
    },
    sliderItem: {
        margin: 10,
        padding: 10,
        paddingLeft: 20,
        paddingRight: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    sliderTextContainer: {
        width: 100,
        minHeight: 50,
        flex: 1,
        justifyContent: 'center',
    },
    sliderText: {
        fontWeight: '500',
        color: Colors.textSecondary,
        textAlign: 'center',
    },
    loaderContainer: {
        margin: 20,
    },
});

const BeerDetails: FunctionComponent<Props> = ({
    navigation,
    addBeerToFavouriteAction,
    removeBeerFromFavouriteAction,
    favouriteBeers,
    similarBeers,
    fetchSimilarBeersAction,
}) => {
    const beer: Beer = navigation.getParam('beer', {});
    useEffect(() => {
        fetchSimilarBeersAction(beer.ibu, beer.id);
    }, [fetchSimilarBeersAction, beer.ibu, beer.id]);
    const isFavourite = Boolean(favouriteBeers.find(favBeer => favBeer.id === beer.id));
    return (
        <ScrollView style={[styles.container]} testID="beer-details">
            <View style={styles.headerContainer}>
                {beer.image_url && (
                    <Image resizeMode="contain" style={styles.image} source={{ uri: beer.image_url }} />
                )}
                <View style={styles.header}>
                    <Text style={[AppStyles.h3, styles.title]}>{beer.name}</Text>
                    <Text style={[AppStyles.h5, styles.tagline]}>{beer.tagline}</Text>
                    <View>
                        <Text style={styles.rate}>
                            IBU: <Text style={styles.greyText}>{beer.ibu}</Text>
                        </Text>
                        <Text style={styles.rate}>
                            ABV: <Text style={styles.greyText}>{beer.abv}%</Text>
                        </Text>
                        <Text style={styles.rate}>
                            EBC: <Text style={styles.greyText}>{beer.ebc}</Text>
                        </Text>
                    </View>
                </View>
            </View>
            <View style={AppStyles.paddingHorizontal}>
                <Spacer size={15} />
                {isFavourite ? (
                    <Button mode="outlined" onPress={() => removeBeerFromFavouriteAction(beer.id)}>
                        Remove from favourites
                    </Button>
                ) : (
                    <Button mode="contained" onPress={() => addBeerToFavouriteAction(beer)}>
                        Add to favourites
                    </Button>
                )}
            </View>
            <View style={styles.textContainer}>
                <Text style={AppStyles.baseText}>{beer.description}</Text>
                <Text style={AppStyles.baseText}>{beer.brewers_tips}</Text>
                <Text style={[AppStyles.h5, styles.bestOptions]}>Best served with:</Text>
                {beer.food_pairing.map(meal => (
                    <Text style={AppStyles.baseText} key={meal}>
                        - {meal}
                    </Text>
                ))}
            </View>
            <View style={styles.textContainer}>
                <Text>You might also like:</Text>
                {similarBeers.status === RequestStatus.PENDING ? (
                    <View style={styles.loaderContainer}>
                        <ActivityIndicator />
                    </View>
                ) : (
                    <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                        {similarBeers.data.map(similarBeer => (
                            <View style={styles.sliderItem} key={similarBeer.id}>
                                <Image
                                    resizeMode="contain"
                                    style={styles.image}
                                    source={
                                        similarBeer.image_url
                                            ? { uri: similarBeer.image_url }
                                            : require('../../assets/images/image-placeholder.jpg')
                                    }
                                />
                                <View style={styles.sliderTextContainer}>
                                    <Text style={styles.sliderText}>{similarBeer.name}</Text>
                                </View>
                            </View>
                        ))}
                    </ScrollView>
                )}
            </View>
        </ScrollView>
    );
};

const mapStateToProps = (store: AppStore) => ({
    favouriteBeers: store.favouriteBeers.data,
    similarBeers: store.similarBeers,
});

export default connect(
    mapStateToProps,
    {
        addBeerToFavouriteAction: addBeerToFavourite,
        removeBeerFromFavouriteAction: removeBeerFromFavourite,
        fetchSimilarBeersAction: fetchSimilarBeers,
    }
)(BeerDetails);
