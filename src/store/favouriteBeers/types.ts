import { Beer } from '../../api/types';
import { RequestStatus } from '../../enums/requestStatus';
import { AddBeerToFavouriteActions } from './actions/addBeerToFavourite/types';
import { RemoveBeerFromFavouriteActions } from './actions/removeBeerFromFavourite/types';
import { FetchFavouriteBeersActions } from './actions/fetchFavouriteBeers/types';

export interface FavouriteBeersReducer {
    data: Beer[];
    status: RequestStatus | null;
    processing: boolean;
    error: null | Error;
}

export type FavouriteBeersActions =
    | AddBeerToFavouriteActions
    | RemoveBeerFromFavouriteActions
    | FetchFavouriteBeersActions;
