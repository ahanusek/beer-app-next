import AsyncStorage from '@react-native-community/async-storage';
import { ThunkAction } from 'redux-thunk';
import { Actions, AppStore, ExtraArguments } from '../../../types';
import { FAVOURITE_BEERS_STORAGE_KEY } from '../../reducer';
import {
    FETCH_FAVOURITE_BEERS_FULFILLED,
    FETCH_FAVOURITE_BEERS_PENDING,
    FETCH_FAVOURITE_BEERS_REJECTED,
    FetchFavouriteBeersFulfilledAction,
    FetchFavouriteBeersPendingAction,
    FetchFavouriteBeersRejectedAction,
} from './types';
import { Beer } from '../../../../api/types';

export const fetchFavouriteBeers = (): ThunkAction<
    void,
    AppStore,
    ExtraArguments,
    Actions
> => async dispatch => {
    dispatch({ type: FETCH_FAVOURITE_BEERS_PENDING } as FetchFavouriteBeersPendingAction);
    try {
        const json = await AsyncStorage.getItem(FAVOURITE_BEERS_STORAGE_KEY);
        const beers: Beer[] = json ? JSON.parse(json) : [];
        dispatch({
            type: FETCH_FAVOURITE_BEERS_FULFILLED,
            payload: beers,
        } as FetchFavouriteBeersFulfilledAction);
    } catch (e) {
        dispatch({
            type: FETCH_FAVOURITE_BEERS_REJECTED,
            payload: e,
        } as FetchFavouriteBeersRejectedAction);
    }
};
