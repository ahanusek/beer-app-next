import { Beer } from '../../../../api/types';

export const FETCH_FAVOURITE_BEERS_PENDING = 'FETCH_FAVOURITE_BEERS_PENDING';
export const FETCH_FAVOURITE_BEERS_FULFILLED = 'FETCH_FAVOURITE_BEERS_FULFILLED';
export const FETCH_FAVOURITE_BEERS_REJECTED = 'FETCH_FAVOURITE_BEERS_REJECTED';

export interface FetchFavouriteBeersPendingAction {
    type: typeof FETCH_FAVOURITE_BEERS_PENDING;
}

export interface FetchFavouriteBeersFulfilledAction {
    type: typeof FETCH_FAVOURITE_BEERS_FULFILLED;
    payload: Beer[];
}

export interface FetchFavouriteBeersRejectedAction {
    type: typeof FETCH_FAVOURITE_BEERS_REJECTED;
    payload: Error;
}

export type FetchFavouriteBeersActions =
    | FetchFavouriteBeersFulfilledAction
    | FetchFavouriteBeersPendingAction
    | FetchFavouriteBeersRejectedAction;
