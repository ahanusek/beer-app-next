import { Beer } from '../../../../api/types';

export const ADD_BEER_TO_FAVOURITE_PENDING = 'ADD_BEER_TO_FAVOURITE_PENDING';
export const ADD_BEER_TO_FAVOURITE_FULFILLED = 'ADD_BEER_TO_FAVOURITE_FULFILLED';
export const ADD_BEER_TO_FAVOURITE_REJECTED = 'ADD_BEER_TO_FAVOURITE_REJECTED';

export interface AddBeerToFavouritePendingAction {
    type: typeof ADD_BEER_TO_FAVOURITE_PENDING;
}

export interface AddBeerToFavouriteFulfilledAction {
    type: typeof ADD_BEER_TO_FAVOURITE_FULFILLED;
    payload: Beer[];
}

export interface AddBeerToFavouriteRejectedAction {
    type: typeof ADD_BEER_TO_FAVOURITE_REJECTED;
    payload: Error;
}

export type AddBeerToFavouriteActions =
    | AddBeerToFavouritePendingAction
    | AddBeerToFavouriteFulfilledAction
    | AddBeerToFavouriteRejectedAction;
