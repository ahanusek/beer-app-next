import AsyncStorage from '@react-native-community/async-storage';
import { ThunkAction } from 'redux-thunk';
import { Beer } from '../../../../api/types';
import { Actions, AppStore, ExtraArguments } from '../../../types';
import {
    ADD_BEER_TO_FAVOURITE_FULFILLED,
    ADD_BEER_TO_FAVOURITE_PENDING,
    ADD_BEER_TO_FAVOURITE_REJECTED,
    AddBeerToFavouriteFulfilledAction,
    AddBeerToFavouritePendingAction,
    AddBeerToFavouriteRejectedAction,
} from './types';
import { FAVOURITE_BEERS_STORAGE_KEY } from '../../reducer';

export const addBeerToFavourite = (
    beer: Beer
): ThunkAction<void, AppStore, ExtraArguments, Actions> => async (dispatch, getState) => {
    dispatch({ type: ADD_BEER_TO_FAVOURITE_PENDING } as AddBeerToFavouritePendingAction);
    const { data } = getState().favouriteBeers;
    try {
        const updatedBeers = [...data, beer];
        await AsyncStorage.setItem(FAVOURITE_BEERS_STORAGE_KEY, JSON.stringify(updatedBeers));
        dispatch({
            type: ADD_BEER_TO_FAVOURITE_FULFILLED,
            payload: updatedBeers,
        } as AddBeerToFavouriteFulfilledAction);
    } catch (e) {
        dispatch({ type: ADD_BEER_TO_FAVOURITE_REJECTED, payload: e } as AddBeerToFavouriteRejectedAction);
    }
};
