import AsyncStorage from '@react-native-community/async-storage';
import { ThunkAction } from 'redux-thunk';
import { Actions, AppStore, ExtraArguments } from '../../../types';
import {
    REMOVE_BEER_FROM_FAVOURITE_FULFILLED,
    REMOVE_BEER_FROM_FAVOURITE_PENDING,
    REMOVE_BEER_FROM_FAVOURITE_REJECTED,
    RemoveBeerFromFavouriteFulfilledAction,
    RemoveBeerFromFavouritePendingAction,
    RemoveBeerFromFavouriteRejectedAction,
} from './types';
import { FAVOURITE_BEERS_STORAGE_KEY } from '../../reducer';

export const removeBeerFromFavourite = (
    beerId: number
): ThunkAction<void, AppStore, ExtraArguments, Actions> => async (dispatch, getState) => {
    dispatch({ type: REMOVE_BEER_FROM_FAVOURITE_PENDING } as RemoveBeerFromFavouritePendingAction);
    const { data } = getState().favouriteBeers;
    try {
        const updatedBeers = data.filter(beer => beer.id !== beerId);
        await AsyncStorage.setItem(FAVOURITE_BEERS_STORAGE_KEY, JSON.stringify(updatedBeers));
        dispatch({
            type: REMOVE_BEER_FROM_FAVOURITE_FULFILLED,
            payload: updatedBeers,
        } as RemoveBeerFromFavouriteFulfilledAction);
    } catch (e) {
        dispatch({
            type: REMOVE_BEER_FROM_FAVOURITE_REJECTED,
            payload: e,
        } as RemoveBeerFromFavouriteRejectedAction);
    }
};
