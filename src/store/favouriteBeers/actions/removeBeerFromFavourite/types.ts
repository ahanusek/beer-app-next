import { Beer } from '../../../../api/types';

export const REMOVE_BEER_FROM_FAVOURITE_PENDING = 'REMOVE_BEER_FROM_FAVOURITE_PENDING';
export const REMOVE_BEER_FROM_FAVOURITE_FULFILLED = 'REMOVE_BEER_FROM_FAVOURITE_FULFILLED';
export const REMOVE_BEER_FROM_FAVOURITE_REJECTED = 'REMOVE_BEER_FROM_FAVOURITE_REJECTED';

export interface RemoveBeerFromFavouritePendingAction {
    type: typeof REMOVE_BEER_FROM_FAVOURITE_PENDING;
}

export interface RemoveBeerFromFavouriteFulfilledAction {
    type: typeof REMOVE_BEER_FROM_FAVOURITE_FULFILLED;
    payload: Beer[];
}

export interface RemoveBeerFromFavouriteRejectedAction {
    type: typeof REMOVE_BEER_FROM_FAVOURITE_REJECTED;
    payload: Error;
}

export type RemoveBeerFromFavouriteActions =
    | RemoveBeerFromFavouritePendingAction
    | RemoveBeerFromFavouriteFulfilledAction
    | RemoveBeerFromFavouriteRejectedAction;
