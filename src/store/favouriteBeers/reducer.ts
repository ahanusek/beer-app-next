import { FavouriteBeersActions, FavouriteBeersReducer } from './types';
import {
    ADD_BEER_TO_FAVOURITE_FULFILLED,
    ADD_BEER_TO_FAVOURITE_PENDING,
    ADD_BEER_TO_FAVOURITE_REJECTED,
} from './actions/addBeerToFavourite/types';
import { RequestStatus } from '../../enums/requestStatus';
import {
    REMOVE_BEER_FROM_FAVOURITE_FULFILLED,
    REMOVE_BEER_FROM_FAVOURITE_PENDING,
    REMOVE_BEER_FROM_FAVOURITE_REJECTED,
} from './actions/removeBeerFromFavourite/types';
import {
    FETCH_FAVOURITE_BEERS_FULFILLED,
    FETCH_FAVOURITE_BEERS_PENDING,
    FETCH_FAVOURITE_BEERS_REJECTED,
} from './actions/fetchFavouriteBeers/types';

const initialState: FavouriteBeersReducer = {
    data: [],
    status: null,
    error: null,
    processing: false,
};

export const FAVOURITE_BEERS_STORAGE_KEY = '@FavouriteBeers';

const favouriteBeers = (state = initialState, action: FavouriteBeersActions): FavouriteBeersReducer => {
    switch (action.type) {
        case ADD_BEER_TO_FAVOURITE_PENDING:
        case REMOVE_BEER_FROM_FAVOURITE_PENDING:
            return {
                ...state,
                processing: true,
                error: null,
            };
        case ADD_BEER_TO_FAVOURITE_FULFILLED:
        case REMOVE_BEER_FROM_FAVOURITE_FULFILLED:
            return {
                ...state,
                processing: false,
                data: action.payload,
            };
        case ADD_BEER_TO_FAVOURITE_REJECTED:
        case REMOVE_BEER_FROM_FAVOURITE_REJECTED:
            return {
                ...state,
                processing: false,
                error: action.payload,
            };
        case FETCH_FAVOURITE_BEERS_PENDING:
            return {
                ...state,
                error: null,
                status: RequestStatus.PENDING,
            };
        case FETCH_FAVOURITE_BEERS_FULFILLED:
            return {
                ...state,
                status: RequestStatus.SUCCESS,
                data: action.payload,
            };
        case FETCH_FAVOURITE_BEERS_REJECTED:
            return {
                ...state,
                error: action.payload,
                status: RequestStatus.ERROR,
            };
        default:
            return state;
    }
};

export default favouriteBeers;
