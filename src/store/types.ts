import { BeerActions, BeersReducer } from './beers/types';
import client from '../api/config';
import { FavouriteBeersActions, FavouriteBeersReducer } from './favouriteBeers/types';
import { SimilarBeersActions, SimilarBeersReducer } from './similarBeers/types';

export interface AppStore {
    beers: BeersReducer;
    favouriteBeers: FavouriteBeersReducer;
    similarBeers: SimilarBeersReducer;
}

export interface ExtraArguments {
    client: typeof client;
}

export type Actions = BeerActions | FavouriteBeersActions | SimilarBeersActions;
