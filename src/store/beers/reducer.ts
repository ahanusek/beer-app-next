import get from 'lodash/get';
import { RequestStatus } from '../../enums/requestStatus';
import { BeerActions, BeersReducer } from './types';
import { FETCH_BEERS_FULFILLED, FETCH_BEERS_PENDING, FETCH_BEERS_REJECTED } from './actions/fetchBeers/types';
import {
    FETCH_MORE_BEERS_FULFILLED,
    FETCH_MORE_BEERS_PENDING,
    FETCH_MORE_BEERS_REJECTED,
    NO_MORE_TO_FETCH,
} from './actions/fetchMoreBeers/types';

const initialState: BeersReducer = {
    status: null,
    data: [],
    error: null,
    page: null,
    fetchingMore: false,
    fetchedAll: false,
};

const beersList = (state: BeersReducer = initialState, action: BeerActions): BeersReducer => {
    switch (action.type) {
        case FETCH_BEERS_PENDING:
            return {
                ...state,
                status: RequestStatus.PENDING,
                error: null,
            };
        case FETCH_BEERS_FULFILLED:
            return {
                ...state,
                status: RequestStatus.SUCCESS,
                data: get(action, 'payload.data', []),
                page: get(action, 'payload.page', 1),
            };
        case FETCH_BEERS_REJECTED:
            return {
                ...state,
                status: RequestStatus.ERROR,
                error: action.payload,
            };
        case FETCH_MORE_BEERS_PENDING:
            return {
                ...state,
                fetchingMore: true,
                error: null,
            };
        case FETCH_MORE_BEERS_FULFILLED:
            return {
                ...state,
                fetchingMore: false,
                data: [...state.data, ...get(action, 'payload.data', [])],
                page: get(action, 'payload.page', 1),
            };
        case FETCH_MORE_BEERS_REJECTED:
            return {
                ...state,
                status: RequestStatus.ERROR,
                error: action.payload,
                fetchingMore: false,
            };
        case NO_MORE_TO_FETCH:
            return {
                ...state,
                fetchingMore: false,
                fetchedAll: true,
            };
        default:
            return state;
    }
};

export default beersList;
