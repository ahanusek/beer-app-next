import { ResPayload } from '../fetchBeers/types';

export const FETCH_MORE_BEERS_PENDING = 'FETCH_MORE_BEERS_PENDING';
export const FETCH_MORE_BEERS_FULFILLED = 'FETCH_MORE_BEERS_FULFILLED';
export const FETCH_MORE_BEERS_REJECTED = 'FETCH_MORE_BEERS_REJECTED';
export const NO_MORE_TO_FETCH = 'NO_MORE_TO_FETCH';

export interface FetchMoreBeersPendingAction {
    type: typeof FETCH_MORE_BEERS_PENDING;
}

export interface FetchMoreBeersFulfilledAction {
    type: typeof FETCH_MORE_BEERS_FULFILLED;
    payload: ResPayload;
}

export interface FetchMoreBeersRejectedAction {
    type: typeof FETCH_MORE_BEERS_REJECTED;
    payload: Error;
}

export interface NoMoreToFetchAction {
    type: typeof NO_MORE_TO_FETCH;
}

export type FetchMoreBeersActions =
    | FetchMoreBeersPendingAction
    | FetchMoreBeersFulfilledAction
    | FetchMoreBeersRejectedAction
    | NoMoreToFetchAction;
