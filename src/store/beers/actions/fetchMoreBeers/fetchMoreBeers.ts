import { ThunkAction } from 'redux-thunk';
import { AxiosResponse } from 'axios';
import { isEmpty } from 'lodash';
import { Actions, AppStore, ExtraArguments } from '../../../types';
import { DEFAULT_PER_PAGE } from '../../../../api/config';
import {
    FETCH_MORE_BEERS_FULFILLED,
    FETCH_MORE_BEERS_PENDING,
    FETCH_MORE_BEERS_REJECTED,
    FetchMoreBeersFulfilledAction,
    FetchMoreBeersPendingAction,
    FetchMoreBeersRejectedAction,
    NO_MORE_TO_FETCH,
    NoMoreToFetchAction,
} from './types';
import { ResPayload } from '../fetchBeers/types';
import { Beer } from '../../../../api/types';

const fetchMoreBeersPending = (): FetchMoreBeersPendingAction => ({
    type: FETCH_MORE_BEERS_PENDING,
});

const fetchMoreBeersFulfilled = (payload: ResPayload): FetchMoreBeersFulfilledAction => ({
    type: FETCH_MORE_BEERS_FULFILLED,
    payload,
});

const fetchMoreBeersRejected = (error: Error): FetchMoreBeersRejectedAction => ({
    type: FETCH_MORE_BEERS_REJECTED,
    payload: error,
});

const noMoreToFetch = (): NoMoreToFetchAction => ({
    type: NO_MORE_TO_FETCH,
});

export const fetchMoreBeers = (): ThunkAction<Promise<void>, AppStore, ExtraArguments, Actions> => async (
    dispatch,
    getState,
    { client }
) => {
    const { page } = getState().beers;
    if (!page) {
        throw new Error();
    }
    dispatch(fetchMoreBeersPending());
    const nextPage = page + 1;
    try {
        const res: AxiosResponse<Beer[]> = await client.get(
            `beers?page=${nextPage}&per_page=${DEFAULT_PER_PAGE}`
        );
        if (isEmpty(res.data)) {
            dispatch(noMoreToFetch());
            return;
        }
        const payload = {
            data: res.data,
            page: nextPage,
        };
        dispatch(fetchMoreBeersFulfilled(payload));
    } catch (e) {
        dispatch(fetchMoreBeersRejected(e));
    }
};
