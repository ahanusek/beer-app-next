import { ThunkAction } from 'redux-thunk';
import { AxiosResponse } from 'axios';
import { DEFAULT_PER_PAGE } from '../../../../api/config';
import { Actions, AppStore, ExtraArguments } from '../../../types';
import {
    FETCH_BEERS_FULFILLED,
    FETCH_BEERS_PENDING,
    FETCH_BEERS_REJECTED,
    FetchBeersFulfilledAction,
    FetchBeersPendingAction,
    FetchBeersRejectedAction,
    ResPayload,
} from './types';
import { Beer } from '../../../../api/types';

const fetchBeersPending = (): FetchBeersPendingAction => ({
    type: FETCH_BEERS_PENDING,
});

const fetchBeersFulfilled = (payload: ResPayload): FetchBeersFulfilledAction => ({
    type: FETCH_BEERS_FULFILLED,
    payload,
});

const fetchBeersRejected = (error: Error): FetchBeersRejectedAction => ({
    type: FETCH_BEERS_REJECTED,
    payload: error,
});

export const fetchBeers = (page = 1): ThunkAction<Promise<void>, AppStore, ExtraArguments, Actions> => async (
    dispatch,
    _,
    { client }
) => {
    dispatch(fetchBeersPending());
    try {
        const res: AxiosResponse<Beer[]> = await client.get(
            `beers?page=${page}&per_page=${DEFAULT_PER_PAGE}`
        );
        const payload = {
            data: res.data,
            page,
        };
        dispatch(fetchBeersFulfilled(payload));
    } catch (err) {
        dispatch(fetchBeersRejected(err));
    }
};
