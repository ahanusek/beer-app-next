import { Beer } from '../../../../api/types';

export const FETCH_BEERS_PENDING = 'FETCH_BEERS_PENDING';
export const FETCH_BEERS_FULFILLED = 'FETCH_BEERS_FULFILLED';
export const FETCH_BEERS_REJECTED = 'FETCH_BEERS_REJECTED';

export interface FetchBeersPendingAction {
    type: typeof FETCH_BEERS_PENDING;
}

export interface ResPayload {
    page: number;
    data: Beer[];
}

export interface FetchBeersFulfilledAction {
    type: typeof FETCH_BEERS_FULFILLED;
    payload: ResPayload;
}

export interface FetchBeersRejectedAction {
    type: typeof FETCH_BEERS_REJECTED;
    payload: Error;
}

export type FetchBeersActions =
    | FetchBeersPendingAction
    | FetchBeersFulfilledAction
    | FetchBeersRejectedAction;
