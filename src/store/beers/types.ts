import { RequestStatus } from '../../enums/requestStatus';
import { FetchBeersActions } from './actions/fetchBeers/types';
import { FetchMoreBeersActions } from './actions/fetchMoreBeers/types';
import { Beer } from '../../api/types';

export interface BeersReducer {
    status: null | RequestStatus;
    data: Beer[];
    error: null | Error;
    page: null | number;
    fetchingMore: boolean;
    fetchedAll: boolean;
}

export type BeerActions = FetchBeersActions | FetchMoreBeersActions;
