import { RequestStatus } from '../../enums/requestStatus';
import { SimilarBeersActions, SimilarBeersReducer } from './types';
import {
    FETCH_SIMILAR_BEERS_FULFILLED,
    FETCH_SIMILAR_BEERS_PENDING,
    FETCH_SIMILAR_BEERS_REJECTED,
} from './actions/fetchSimilarBeers/types';

const initialState: SimilarBeersReducer = {
    status: null,
    data: [],
    error: null,
};

const similarBeers = (state = initialState, action: SimilarBeersActions): SimilarBeersReducer => {
    switch (action.type) {
        case FETCH_SIMILAR_BEERS_PENDING:
            return {
                ...state,
                status: RequestStatus.PENDING,
                error: null,
            };
        case FETCH_SIMILAR_BEERS_FULFILLED:
            return {
                ...state,
                status: RequestStatus.SUCCESS,
                data: action.payload,
            };
        case FETCH_SIMILAR_BEERS_REJECTED:
            return {
                ...state,
                status: RequestStatus.ERROR,
                error: action.payload,
            };
        default:
            return state;
    }
};

export default similarBeers;
