import { RequestStatus } from '../../enums/requestStatus';
import { Beer } from '../../api/types';
import { FetchSimilarBeersActions } from './actions/fetchSimilarBeers/types';

export interface SimilarBeersReducer {
    error: null | Error;
    data: Beer[];
    status: null | RequestStatus;
}

export type SimilarBeersActions = FetchSimilarBeersActions;
