import { Beer } from '../../../../api/types';

export const FETCH_SIMILAR_BEERS_PENDING = 'FETCH_SIMILAR_BEERS_PENDING';
export const FETCH_SIMILAR_BEERS_FULFILLED = 'FETCH_SIMILAR_BEERS_FULFILLED';
export const FETCH_SIMILAR_BEERS_REJECTED = 'FETCH_SIMILAR_BEERS_REJECTED';

export interface FetchSimilarBeersPendingAction {
    type: typeof FETCH_SIMILAR_BEERS_PENDING;
}

export interface FetchSimilarBeersFulfilledAction {
    type: typeof FETCH_SIMILAR_BEERS_FULFILLED;
    payload: Beer[];
}

export interface FetchSimilarBeersRejectedAction {
    type: typeof FETCH_SIMILAR_BEERS_REJECTED;
    payload: Error;
}

export type FetchSimilarBeersActions =
    | FetchSimilarBeersPendingAction
    | FetchSimilarBeersFulfilledAction
    | FetchSimilarBeersRejectedAction;
