import { AxiosResponse } from 'axios';
import { ThunkAction } from 'redux-thunk';
import { Actions, AppStore, ExtraArguments } from '../../../types';
import { Beer } from '../../../../api/types';
import {
    FETCH_SIMILAR_BEERS_FULFILLED,
    FETCH_SIMILAR_BEERS_PENDING,
    FETCH_SIMILAR_BEERS_REJECTED,
    FetchSimilarBeersPendingAction,
    FetchSimilarBeersRejectedAction,
} from './types';

const PER_PAGE = 4;
const PAGE = 1;
const IBU_RANGE = 15;

export const fetchSimilarBeers = (
    ibu: number,
    id: number
): ThunkAction<Promise<void>, AppStore, ExtraArguments, Actions> => async (dispatch, _, { client }) => {
    dispatch({ type: FETCH_SIMILAR_BEERS_PENDING } as FetchSimilarBeersPendingAction);
    const minIbu = ibu - IBU_RANGE > 0 ? ibu - IBU_RANGE : 1;
    const maxIbu = ibu + IBU_RANGE;
    try {
        const res: AxiosResponse<Beer[]> = await client.get(
            `beers?page=${PAGE}&per_page=${PER_PAGE}&ibu_gt=${minIbu}&ibu_lt=${maxIbu}`
        );
        const filteredBeers = res.data.filter(beer => beer.id !== id).filter((__, index) => index < 3);
        dispatch({ type: FETCH_SIMILAR_BEERS_FULFILLED, payload: filteredBeers });
    } catch (e) {
        dispatch({ type: FETCH_SIMILAR_BEERS_REJECTED, payload: e } as FetchSimilarBeersRejectedAction);
    }
};
