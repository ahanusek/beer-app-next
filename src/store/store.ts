import { combineReducers, applyMiddleware, compose, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import client from '../api/config';
import beers from './beers/reducer';
import favouriteBeers from './favouriteBeers/reducer';
import similarBeers from './similarBeers/reducer';

const appReducer = combineReducers({
    beers,
    favouriteBeers,
    similarBeers,
});

let middleware = [thunk.withExtraArgument({ client })];
// eslint-disable-next-line
if (__DEV__) {
    // Dev-only middleware
    middleware = [...middleware, createLogger()];
}

const store = compose(applyMiddleware(...middleware))(createStore)(appReducer);

export default store;
