import Colors from './colors';
import Fonts from './fonts';
import Sizes from './sizes';

export default {
    emptyList: {
        marginTop: 30,
        padding: 10,
    },
    appContainer: {
        backgroundColor: Colors.background,
    },
    // Default
    container: {
        position: 'relative' as 'relative',
        flex: 1,
        flexDirection: 'column' as 'column',
        backgroundColor: Colors.background,
    },
    containerCentered: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    rowContainer: {
        alignItems: 'center' as 'center',
        flexDirection: 'row' as 'row',
    },
    windowSize: {
        height: Sizes.screen.height,
        width: Sizes.screen.width,
    },

    // Aligning items
    leftAligned: {
        alignItems: 'flex-start',
    },
    centerAligned: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    rightAligned: {
        alignItems: 'flex-end',
    },

    // Text Styles
    baseText: {
        fontFamily: Fonts.base.family,
        fontSize: Fonts.base.size,
        lineHeight: Fonts.base.lineHeight,
        color: Colors.textPrimary,
    },
    secondaryText: {
        fontFamily: Fonts.base.family,
        fontSize: Fonts.base.size,
        color: Colors.textFourth,
    },
    p: {
        fontFamily: Fonts.base.family,
        fontSize: Fonts.base.size,
        lineHeight: Fonts.base.lineHeight,
        color: Colors.textPrimary,
        marginBottom: 8,
    },
    bigHeader: {
        fontSize: Fonts.h1.size * 1.2,
        lineHeight: Fonts.h1.lineHeight * 1.2,
        color: Colors.textPrimary,
        marginVertical: 5,
        left: 0,
        right: 0,
    },
    h1: {
        fontWeight: 'bold',
        fontSize: Fonts.h1.size,
        lineHeight: Fonts.h1.lineHeight,
        color: Colors.textPrimary,
        margin: 0,
        left: 0,
        right: 0,
    },
    h2: {
        fontWeight: 'bold',
        fontSize: Fonts.h2.size,
        lineHeight: Fonts.h2.lineHeight,
        color: Colors.textPrimary,
        margin: 0,
        marginBottom: 4,
        left: 0,
        right: 0,
    },
    h3: {
        fontWeight: 'bold' as 'bold',
        fontSize: Fonts.h3.size,
        lineHeight: Fonts.h3.lineHeight,
        color: Colors.textPrimary,
        margin: 0,
        marginBottom: 4,
        left: 0,
        right: 0,
    },
    h4: {
        fontSize: Fonts.h4.size,
        lineHeight: Fonts.h4.lineHeight,
        color: Colors.textPrimary,
        margin: 0,
        marginBottom: 4,
        left: 0,
        right: 0,
    },
    h5: {
        fontSize: Fonts.h5.size,
        lineHeight: Fonts.h5.lineHeight,
        color: Colors.textPrimary,
        margin: 0,
        marginTop: 4,
        marginBottom: 4,
        left: 0,
        right: 0,
    },
    strong: {
        fontWeight: '700',
    },
    medium: {
        fontWeight: '500',
    },
    light: {
        fontFamily: '300',
    },
    link: {
        textDecorationLine: 'underline',
    },
    subtext: {
        fontFamily: 'Quicksand-Light',
        fontSize: Fonts.base.size * 1,
        lineHeight: Math.floor(Fonts.base.lineHeight * 0.8),
        color: Colors.textSecondary,
    },

    // Helper Text Styles
    textCenterAligned: {
        textAlign: 'center',
    },
    textRightAligned: {
        textAlign: 'right',
    },

    // Give me padding
    padding: {
        paddingVertical: Sizes.padding,
        paddingHorizontal: Sizes.padding,
    },
    paddingHorizontal: {
        paddingHorizontal: Sizes.padding,
    },
    paddingHorizontalBig: {
        paddingHorizontal: Sizes.padding * 1.5,
    },
    paddingLeft: {
        paddingLeft: Sizes.padding,
    },
    paddingRight: {
        paddingRight: Sizes.padding,
    },
    paddingVertical: {
        paddingVertical: Sizes.padding,
    },
    paddingVerticalBig: {
        paddingVertical: Sizes.padding * 1.5,
    },
    paddingTop: {
        paddingTop: Sizes.padding,
    },
    paddingBottom: {
        paddingBottom: Sizes.padding,
    },
    paddingSml: {
        paddingVertical: Sizes.paddingSml,
        paddingHorizontal: Sizes.paddingSml,
    },
    paddingHorizontalSml: {
        paddingHorizontal: Sizes.paddingSml,
    },
    paddingLeftSml: {
        paddingLeft: Sizes.paddingSml,
    },
    paddingRightSml: {
        paddingRight: Sizes.paddingSml,
    },
    paddingVerticalSml: {
        paddingVertical: Sizes.paddingSml,
    },
    paddingTopSml: {
        paddingTop: Sizes.paddingSml,
    },
    paddingBottomSml: {
        paddingBottom: Sizes.paddingSml,
    },

    // General HTML-like Elements
    hr: {
        left: 0,
        right: 0,
        borderBottomWidth: 1,
        height: 1,
        backgroundColor: 'transparent',
        marginTop: Sizes.padding,
        marginBottom: Sizes.padding,
    },

    // Grid
    row: {
        left: 0,
        right: 0,
        flexDirection: 'row' as 'row',
    },
    flex1: {
        flex: 1,
    },
    flex2: {
        flex: 2,
    },
    flex3: {
        flex: 3,
    },
    flex4: {
        flex: 4,
    },
    flex5: {
        flex: 5,
    },
    flex6: {
        flex: 6,
    },

    // Navbar
    navbar: {
        backgroundColor: Colors.light,
        borderBottomWidth: 0,
        height: Sizes.navbarHeight,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 2 },
        shadowOpacity: 0.2,
    },
    navbarTitleContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    navbarTitle: {
        color: Colors.textPrimary,
        fontFamily: 'Quicksand-Bold',
        fontSize: 16,
    },
    shadow: {
        elevation: 6,
        shadowColor: Colors.primary3,
        shadowOffset: { width: 6, height: 6 },
        shadowOpacity: 0.2,
    },
};
