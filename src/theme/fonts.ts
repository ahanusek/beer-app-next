import { Platform } from 'react-native';

export function lineHeight(fontSize: number): number {
    const multiplier = fontSize > 20 ? 0.1 : 0.33;
    return Math.floor(fontSize + fontSize * multiplier);
}

const base = {
    size: 14,
    lineHeight: lineHeight(14),
    ...Platform.select({
        ios: {
            family: 'HelveticaNeue',
        },
        android: {
            family: 'Roboto',
        },
    }),
};

const bold = {
    fontFamily: 'Quicksand-Bold',
};

const medium = {
    fontFamily: 'Quicksand-Medium',
};

const light = {
    fontFamily: 'Quicksand-Light',
};

export default {
    bold: { ...bold },
    light,
    medium,
    base: { ...base },
    h1: { ...base, size: base.size * 1.75, lineHeight: lineHeight(base.size * 2) },
    h2: { ...base, size: base.size * 1.5, lineHeight: lineHeight(base.size * 1.75) },
    h3: { ...base, size: base.size * 1.25, lineHeight: lineHeight(base.size * 1.5) },
    h4: { ...base, size: base.size * 1.1, lineHeight: lineHeight(base.size * 1.25) },
    h5: { ...base },
};
