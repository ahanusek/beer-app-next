import { Dimensions, Platform } from 'react-native';

const { width, height } = Dimensions.get('window');
const screenHeight = width < height ? height : width;
const screenWidth = width < height ? width : height;

export default {
    // Window Dimensions
    screen: {
        height: screenHeight,
        width: screenWidth,

        widthHalf: screenWidth * 0.5,
        widthThird: screenWidth * 0.333,
        widthTwoThirds: screenWidth * 0.666,
        widthQuarter: screenWidth * 0.25,
        widthThreeQuarters: screenWidth * 0.75,
    },
    navbarHeight: Platform.OS === 'ios' ? 74 : 64,
    statusBarHeight: Platform.OS === 'ios' ? 16 : 0,
    tabbarHeight: 51,

    padding: 30,
    paddingSml: 15,

    margin: 30,

    borderRadius: 24,
    smlBorderRadius: 12,
};
