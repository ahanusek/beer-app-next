export const colors = {
    light: '#ffffff',
    primary1: '#4043d5',
    primary2: '#6584e7',
    primary3: '#849dff',
    primary4: '#e8e9fe',
    primary5: '#f5f3fd',
    secondary1: '#ef2455',
    secondary2: '#ff5485',
    secondary3: '#fc7463',
    secondary4: '#fdad8b',
    textPrimary: '#2a2a47',
    textSecondary: '#8f8ca0',
    textFourth: 'rgba(143,140,160, 0.6)',
    textLight: '#ffffff',
    textThird: '#6584e7',
    gradients: {
        primary: ['#434cd4', '#607ae1'],
        secondary: ['#8fa6ff', '#b6caff'],
        third: ['#ff4c80', '#ff96b9'],
        fourth: ['#f56b74', '#fea28b'],
    },
    shadowColor: '#849dff',
    background: '#f5f3fd',
    disabled: 'rgba(224, 224, 226, 0.8)',
    error: '#ED1C24',
};

export default colors;
