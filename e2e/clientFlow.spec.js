describe('BeerApp initial flow', () => {
    beforeEach(async () => {
        await device.reloadReactNative();
    });

    it('should have beers list', async () => {
        await expect(element(by.id('beers-list'))).toBeVisible();
    });

    it('should go to beer details', async () => {
        await expect(element(by.id('beers-list'))).toBeVisible();
        await element(by.id('see-details').withAncestor(by.id('beer-card-1'))).tap();
        await expect(element(by.id('beer-details'))).toBeVisible();
    });
    it('should add beer to favourite', async () => {
        await expect(element(by.id('beers-list'))).toBeVisible();
        await element(by.id('icon-button').withAncestor(by.id('beer-card-1'))).tap();
        await element(by.id('favs-list-tab')).tap();
        await expect(element(by.id('favourite-beers'))).toBeVisible();
        await waitFor(element(by.id('beer-card-1')))
            .toBeVisible()
            .withTimeout(2000);
    });
    it('should remove beer from favourite', async () => {
        await expect(element(by.id('beers-list'))).toBeVisible();
        await element(by.id('favs-list-tab')).tap();
        await expect(element(by.id('favourite-beers'))).toBeVisible();
        await waitFor(element(by.id('beer-card-1')))
            .toBeVisible()
            .withTimeout(2000);
        await element(by.id('icon-button'))
            .atIndex(0)
            .tap();
        await expect(element(by.id('beer-card-1'))).toBeNotVisible();
        await expect(element(by.id('empty-favs-list'))).toBeVisible();
    });
});
