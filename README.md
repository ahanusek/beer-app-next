## About BeerApp

This is simply application which allow you extend knowledge about beers and find beer which perfect fit to you.
I also added feature which allow adding beer to your list of favourite beers.

## How to run app ?

#### `yarn install`

Install all npm dependencies for app

#### `cd ios && pod install`

Install required pods for iOS app

#### `react-native run-ios`

Runs your app in development mode on iOS.

#### `react-native run-android`

Runs your app in development mode on Android.

## How to run integration / unit tests ?

#### `yarn test`

## How to run e2e test ?

Read about detox and add required dependecies, then:

#### `detox build`

and

#### `detox test`

## Some Screens

<img src="https://i.imgur.com/2xNaKFD.png" width="300">
<img src="https://i.imgur.com/gEw8Uy6.png" width="300">
<img src="https://i.imgur.com/bPA7MwD.png" width="300">
